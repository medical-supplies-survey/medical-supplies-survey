const express = require("express");
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./src/config");

app.use(cors());
app.use(express.urlencoded({extended:false}));
app.use(express.json());

app.listen(config.port,()=>{
  console.log(`Connected to port ${config.port}`);
});

mongoose.connect(config.dbURL,{ useNewUrlParser: true,useUnifiedTopology: true })
.then(console.log("Database Connected"));

const hospitalRoute = require("./src/routes/hospitalRoute");
const contactRouter = require("./src/routes/contactRoute");
app.use("/",[hospitalRoute,contactRouter]);