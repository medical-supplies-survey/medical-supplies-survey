const {Schema,model} = require("mongoose");

const HospitalSchema = new Schema({
    name:String,
    contact:Number,
    address:String,
    city:String
});

module.exports = model("Hospital",HospitalSchema);