const {Schema,model} = require("mongoose");

const ContactSchema = new Schema({
    department: String,
    contact:[{type:Number}],
    hospital: {
        type:Schema.Types.ObjectId,
        ref:"Hospital"
    }
});

module.exports=model("Contact",ContactSchema);