const express = require("express");
const Router = express.Router();
const Model = require("../models/Hospital");

Router.get("/hospitals", async(req,res)=>{
    const hospitals = await Model.find();
    res.send(hospitals);
});

Router.get("/hospital",async(req,res)=>{
    const hospital = await Model.findById(req.body.id);
    res.send(hospital);
});

Router.post("/addhospital",async(req,res)=>{
    console.log(req.body)
    try{
        const hospital = new Model({
            name: req.body.name,
            contact: req.body.contact,
            address: req.body.address,
            city: req.body.city
        });
        hospital.save();
        res.send(hospital);
    }catch(e){
        res.status(404).send("error in adding new hospital");
    }
});

Router.delete("/deletehospital",async(req,res)=>{
    try{
        const hospital = await Model.findByIdAndDelete(req.body.id);
        res.send(hospital);
    }catch(e){
        res.status(404).send("error in adding new hospital");
    }
});

Router.patch("/edithospital",async(req,res)=>{
    try{
        const hospital = await Model.findById(req.body.id);
        hospital.name = req.body.name;
        hospital.contact = req.body.contact;
        hospital.address = req.body.address;
        hospital.city = req.body.city;
        hospital.save();
        
        res.send(hospital);
    }catch(e){
        res.status(404).send("error in editing hospital");
    }
})

module.exports = Router;