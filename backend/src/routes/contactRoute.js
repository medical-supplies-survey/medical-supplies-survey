const express = require("express");
const Router = express.Router();
const Model = require("../models/Contact");

Router.get("/contacts", async(req,res)=>{
    const contacts = Model.find();
    res.send(contacts);
});


module.exports = Router;