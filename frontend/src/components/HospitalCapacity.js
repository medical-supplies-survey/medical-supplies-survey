import React, {useState} from 'react';
import {Table, Input, Label, TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Form, ButtonDropdown, DropdownMenu, ListGroupItem, DropdownItem, DropdownToggle, Dropdown} from 'reactstrap';
import classnames from 'classnames';

const HospitalCapacity = (props) => {
        const [activeTab, setActiveTab] = useState('');

        const [showDropdown,setShowDropdown]=useState(false);

        const [items, setItems] =useState([]);

       const toggle = tab => {
          if(activeTab !== tab) setActiveTab(tab);
        }

        const categories = [
            {category:"TestingKits",items:["Molecular Covid-19 Testing Kits", "Serology Covid-19 Testing Kits"]},
            {category:"Personal Protective Equipment",items:[" N-95 Respirator Masks", "Protective Suits", "Face Shields", "Surgical Masks", "Gloves"]},
            {category:"FDA Approved Ventilators",items:["PAP Ventilators (Positive Airway Pressure)", " Intubation Ventilator"]},
            {category:"Patient Care",items:["Thermometer Scanners"]},
            {category:"Cleaning / Disinfecting Supplies",items:["Gallons of Antibacterial Hand Soap (sink)", " Gallons of Hand Sanitizer (dispenser based)t"]}
        ]
    
    

    const handleItem=(index)=>{

        setItems(categories[index].items)
        
    }


    return (
        <>

    <div>
      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          >
            Medical Equipment Supplies
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
            Hospital Capacity
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
          >
            Hospital Staff Capabilities
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <Row>
            <Col sm="12">
            <h1 className = 'text-center'>Medical Equipment Supplies</h1>

                    <Table striped>
                        <thead>
                            <tr>
                            <th>
                            <Dropdown isOpen={showDropdown} toggle={()=>setShowDropdown(!showDropdown)}>
                            <DropdownToggle caret>Categories</DropdownToggle>
                            <DropdownMenu>
                                {categories.map((category,index)=>(
                                    <DropdownItem 
                                    key={index}
                                    onClick={()=>handleItem(index)}
                                    >{category.category}</DropdownItem>
                                ))}
                                
                            </DropdownMenu>
                            </Dropdown>
                            </th>
                            <th>Current Number</th>
                            <th>Expected within 7 Days</th>
                            </tr>
                        </thead>
                        <tbody>
                                       
                                {items.map((item, index)=>(
                                  <tr>
                                    <td>
                                    <ListGroupItem
                                        onClick = {() => setItems(item)}
                                        key = {index}
                                    >{item}</ListGroupItem>
                                    </td>
                                    <td>
                                        <Input/>
                                    </td>
                                    <td>
                                        <Input/>
                                    </td>
                                  </tr>
                               
                                ))}
                         
                           
                        </tbody>
                    </Table>

                <Button>Submit</Button>
            </Col>

          </Row>
        </TabPane>
        <TabPane tabId="2">
          <Row>
          <Col sm="12">
            <h1 className = 'text-center'>Hospital Capacity</h1>
                    <Table striped>
                        <thead>
                            <tr>
                            <th></th>
                            <th></th>
                            <th>Current Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <th scope="row">1</th>
                            <td>Total ICU beds (not including pediatric and NICU beds)</td>
                            <td scope="row"><input className = 'text-center'/></td>
                            </tr>
                            <tr>
                            <th scope="row">2</th>
                            <td>ICU beds currently open (not including pediatric and NICU beds)</td>
                            <td scope="row"><input className = 'text-center'/></td>
                            </tr>
                            <tr>
                            <th scope="row">3</th>
                            <td>Total Staffed Beds</td>
                            <td scope="row"><input className = 'text-center'/></td>
                            </tr>
                            <tr>
                            <th scope="row">4</th>
                            <td>Staffed Beds currently open</td>
                            <td scope="row"><input className = 'text-center'/></td>
                            </tr>
                            <tr>
                            <th scope="row">5</th>
                            <td>Total Non-ICU Beds</td>
                            <td scope="row"><input className = 'text-center'/></td>
                            </tr>
                            <tr>
                            <th scope="row">6</th>
                            <td>Non-ICU Beds currently open</td>
                            <td scope="row"><input className = 'text-center'/></td>
                            </tr>
                            <tr>
                            <th scope="row">7</th>
                            <td>Covid-19 patients in the hospital</td>
                            <td scope="row"><input className = 'text-center'/></td>
                            </tr>
                        </tbody>
                    </Table>
                <Button>Submit</Button>
            </Col>
          </Row>
        </TabPane>
        <TabPane tabId="3">
          <Row>
          <Col sm="12">
            <h1 className = 'text-center'>Hospital Staff Capabilities</h1>
                    <Table striped>
                <thead>
                    <tr>
                    <th></th>
                    <th></th>
                    <th>Current Number</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td>Total Active Doctors</td>
                    <td scope="row"><input className = 'text-center'/></td>
                    </tr>
                    <tr>
                    <th scope="row">2</th>
                    <td>Total Doctors Infected</td>
                    <td scope="row"><input className = 'text-center'/></td>
                    </tr>
                    <tr>
                    <th scope="row">3</th>
                    <td>Total Active Physician Assistants</td>
                    <td scope="row"><input className = 'text-center'/></td>
                    </tr>
                    <tr>
                    <th scope="row">3</th>
                    <td>Total Physician Assistants Infected</td>
                    <td scope="row"><input className = 'text-center'/></td>
                    </tr>
                    <tr>
                    <th scope="row">5</th>
                    <td>Total Active Nurses</td>
                    <td scope="row"><input className = 'text-center'/></td>
                    </tr>
                    <tr>
                    <th scope="row">6</th>
                    <td>Total Nurses Infected</td>
                    <td scope="row"><input className = 'text-center'/></td>
                    </tr>
                </tbody>
                </Table>
                <Button>Finish Survey</Button>
            </Col>
          </Row>
        </TabPane>
      </TabContent>
    </div>

        </>
    )
}

export default HospitalCapacity;