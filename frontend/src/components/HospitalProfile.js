import React from 'react';
import Hospitals from './Hospitals';

const HospitalProfile = ({hospital}) => {

    return(
        <div
            className = 'd-flex'
        >
        
        <div
            className = 'vh-100 w-50'
        >
            <h3>{hospital.name}</h3>
            <p>{hospital.contact}</p>
            <p>{hospital.address}</p>
            <p>{hospital.city}</p>
        </div>
        <div
            className = 'vh-100 w-50'
        >

        </div>
        </div>
    )
} 

export default HospitalProfile;