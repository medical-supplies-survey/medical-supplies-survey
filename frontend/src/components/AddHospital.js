import React, {useState} from 'react';
import {Modal, ModalHeader, ModalBody, Button, ModalFooter, Label, Input} from 'reactstrap'
;
import axios from 'axios';
const AddHospital = ({setShowForm, showForm, handleAddHospital}) => {

const [name, setName] = useState ("");
const [contact, setContact] = useState ("");
const [address, setAddress] = useState ("");
const [city, setCity] = useState ("");

    return(
        <Modal
            isOpen = {showForm}
        >
            <ModalHeader
                className = 'justify-content-center d-flex'
                toggle = {() => setShowForm(!showForm)}
            >Add Hospital</ModalHeader>
            <ModalBody
                className = 'w-75 mx-auto'
            >
                <Label>Hospital: </Label>
                <Input 
                    onChange = {(e) => setName(e.target.value)}
                />
                <Label>Contact: </Label>
                <Input 
                    onChange = {(e) => setContact(e.target.value)}
                />
                <Label>Address: </Label>
                <Input 
                    onChange = {(e) => setAddress(e.target.value)}
                />
                <Label>City: </Label>
                <Input 
                    onChange = {(e) => setCity(e.target.value)}
                />
            </ModalBody>
            <ModalFooter
                className = 'justify-content-center d-flex'
            >
                <Button
                    onClick = {() => {handleAddHospital(name, contact, address, city)
                        setName ("")
                        setContact ("")
                        setAddress("")
                        setCity ("")
                        setShowForm (false)
                    }}
                >Add</Button>
            </ModalFooter>
        </Modal>
    )
}

export default AddHospital;