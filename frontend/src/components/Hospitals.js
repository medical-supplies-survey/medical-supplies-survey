import React, {useState, useEffect} from "react";
import {ListGroup, ListGroupItem, Button} from 'reactstrap';
import AddHospital from './AddHospital';
import axios from 'axios';
import HospitalCapacity from "./HospitalCapacity";


const Hospitals = ({hospital, setHospital}) => {
  const [showForm, setShowForm] = useState(false);
  const [hospitals, setHospitals] = useState([]);

  useEffect(() =>{
    axios('http://localhost:4000/hospitals') .then(res => {
        setHospitals(res.data)
    })
  }, [])

const handleAddHospital = (name, contact, address, city) => {

    axios.post ('http://localhost:4000/addhospital', {name, contact, address, city}) .then (res => {
        axios('http://localhost:4000/hospitals') .then(res => {
            setHospitals(res.data)
        })
      })
    }

  return (
    <>
    <h1 className = 'text-center'>List of Hospitals</h1>
    <div
        className = 'row'
    >
         <ListGroup
            className = 'col-6 offset-3 text-center'
        >
            {
                hospitals.map((hospital, index) =>(
                <ListGroupItem
                    onClick = {() => setHospital(hospital)}
                    key = {index}
                >{hospital.name}</ListGroupItem>
                ))
            }
        <HospitalCapacity />
        </ListGroup>
        <div
            className = 'col-3'
        >
            <Button
                onClick = {() => setShowForm(!showForm)} 
            >Add Hospital</Button>
            <AddHospital 
                setShowForm = {setShowForm}
                showForm = {showForm}
                handleAddHospital = {handleAddHospital}
            />
        </div>
    </div>
    </>
  );
};

export default Hospitals;
