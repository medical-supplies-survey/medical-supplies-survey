import React, {useState} from "react";
import Hospitals from './components/Hospitals';
import HospitalProfile from './components/HospitalProfile';


const App = () => {
  const [hospital, setHospital] = useState ({})
  return (
    <>
    <Hospitals 
      setHospital = {setHospital}
    />
    <HospitalProfile 
      hospital = {hospital}
    />
    </>
  );
};

export default App;
